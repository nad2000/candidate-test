#!/usr/bin/env python
import os
import time
import random
import sqlite3


DB_NAME = 'alarms.db'
DEVICE_COUNT = 3
ALARM_COUNT = 2


def write_event(timestamp_utc, device_name, alarm_severity, alarm_name):
    connection = sqlite3.connect(DB_NAME)
    cursor = connection.cursor()
    cursor.execute('''
    insert into
        alarm_event (timestamp_utc, device_name, alarm_name, alarm_severity)
        values (?, ?, ?, ?)
    ''', (timestamp_utc, device_name, alarm_name, alarm_severity))
    connection.commit()
    connection.close()


def main():
    device_alarms = {}

    while True:
        for device_number in range(DEVICE_COUNT):
            timestamp_utc = int(time.time())
            device_name = "TEST%d.DEV.AVN.MGNT" % device_number

            for alarm_number in range(ALARM_COUNT):
                alarm_severity = random.choice(range(6))
                alarm_name = "ALARM.%d" % alarm_number
                write_event(timestamp_utc, device_name, alarm_severity, alarm_name)

                time.sleep(1)


if __name__ == '__main__':
    print 'writing alarm events to %s' % DB_NAME
    try:
        main()
    except KeyboardInterrupt:
        print 'done'
