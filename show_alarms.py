#!/usr/bin/env python
"""
Solution to task 3

@author: rad@nowitworks.eu
@url: http://hg.nowitworks.eu/candidate-test

"""

from alarm_stream import DB_NAME, DEVICE_COUNT
import sqlite3
import sys


def known_devices():
    """Generates lables of all known devices"""

    for device_number in xrange(DEVICE_COUNT):
        yield "TEST%d.DEV.AVN.MGNT" % device_number


def show_stats():
    """Shows alarm stats"""

    connection = sqlite3.connect(DB_NAME)
    cursor = connection.cursor()

    cursor.execute("""
        SELECT device_name, alarm_severity, count(*) AS alarm_count
        FROM current_alarm
        GROUP BY device_name, alarm_severity;""")
    current_alarm_counts = {(dev, severity): count
                            for (dev, severity, count) in cursor.fetchall()}

    # Assuming that "the previous 5 minute period" means 5min upto the current
    # time otherwise we should query 'sla_stats':
    cursor.execute("""
        SELECT device_name,
            (MIN( strftime('%s', 'now') - timestamp_utc ) / 300.0) * 100
        FROM alarm_event
        WHERE alarm_severity = 5
          AND timestamp_utc >= (strftime('%s', 'now') - 300.0)
        GROUP BY device_name;""")
    sla_stats = dict(cursor.fetchall())

    # All severities except CLEARED
    cursor.execute("SELECT * FROM severity WHERE severity_id != 0;")
    severities = dict(cursor.fetchall())

    cursor.close()
    connection.close()

    # Print header:
    print (" {device:^23s}" + " | {:^11s} " * len(severities) + " |  {sla:^6s} ").format(
        device='Device', *severities.values(), sla='SLA')
    print '=' * (25 + len(severities) * 17)

    # Print stats:
    for dev in known_devices():

        dev_severities = [current_alarm_counts.get((dev, severity), 0)
                          for severity in severities]

        print (" {device:23s}" + " | {:11d} " * len(severities) + " |  {sla:6.2f} ").format(
            device=dev,
            *dev_severities,
            sla=sla_stats.get(dev, 100.))


def show_usage():
    """Shows the application usage"""

    print """Usage: %s -h | --help | -? | -s | --show

This terminal application displays a table of all dknown devices.
Within this table it show the following information:

    - device name;
    - the number and (human readable) severity of any current alarms;
    - SLA for the previous 5 minute period expressed as a percentage.

  -s, --show       shows statistics
  -h, -?, --help   display this help and exit
""" % sys.argv[0]

if __name__ == '__main__':
    # NOTE: since the interface is so simple, I opted not to use 'argparse':
    if len(sys.argv) < 2 or any(op in sys.argv for op in ['-?', '-h', '--help']) \
            or not('-s' in sys.argv or '--show' in sys.argv):
        show_usage()
    else:
        show_stats()
