Candidate technical exercise
============================

This exercise involves creating a fictional alarm event system.

In practise, an alarm represents a warning or failure condition with a remote device.
Alarm events are the events that dictate whether the alarm is being created, updated or cleared.

A number of files have been supplied alongside this file, as well as an initialised empty sqlite database file.
Python has builtin support for sqlite database files, hence no third party libraries are to be used.

Your response should include all the files sent to you (in a completed state), including the modified database file.
Your response code should be complete to a level that you would be comfortable deploying to a production server.

Supplied files
==============

README
------
This file, describes the exercise.

alarms.db
---------
The sqlite3 database used for this exercise.

This database will be used by multiple processes concurrently.
Due to this, there is a chance that an OperationalError exception will be thrown if the database is locked.

alarm_stream.py
---------------
A simple test alarm event generator which generates a random alarm event every second.

When your solution is tested, the alarm_stream.py script will be left running.
The alarm_stream.py script continuously inserts rows into the alarm_event table.
The alarm_stream.py script is not intended as an example of 'good' Python code.

alarm_processor.py
------------------
This file will contain your solution to tasks 1 and 2.

show_alarms.py
--------------
This file will contain your solution to task 3.


Task 1 - Maintain current alarm table
=====================================

Write code that processes alarm events as they occur and maintains the current_alarm table of alarms.
This table reflects the current state of all alarms that have not yet been cleared.

To determine if a new alarm event matches an existing current alarm the 'device_name' and 'alarm_name' fields are compared.
If they match, they refer to the same alarm.

An alarm event can have a severity in the range 0 - 5:
0: CLEARED
1: SUPERVISORY
2: WARNING
3: MINOR
4: MAJOR
5: CRITICAL

An alarm is considered to be 'cleared' if the severity is 0.
Cleared alarms are no longer considered current, so need to be removed from the current_alarm table.
All other values are in ascending order of severity, with CRITICAL being the most severe.

An alarm is considered to be 'updated' if the severity changes but the device_name and alarm_name remain unchanged.
An updated alarm replaces the existing current alarm.

Of course, any new alarms are just added to the table.

Duplicate current alarms are not allowed.
The solution should ensure that any alarm events are continuously processed and that the current alarm table is continuously updated, i.e. not exit until Ctrl-C is pressed.


Task 2 - Statistics extension
=============================

Extend your solution to include some basic metrics for each device.
The list of all devices can be be deduced by looking at the alarm_stream.py script.

These metrics must be stored in the same database as the alarm events.
Changes to the schema of the database must be clearly identified.

Each metric value must be computed and stored using a 5 minute interval.
A maximum of 3 days of historical metric values must be stored - any older values must be deleted.

1. Alarm count
    The number of alarm events received per device, per severity.
2. SLA
    The amount of time that the device did *not* have a CRITICAL alarm present in the current alarm table for the metric interval.


Task 3 - Simple user interface
==============================

Using the provided show_alarms.py file, create a terminal (command line) application.

This terminal application must display a table of all known devices.
Within this table the following information is required:
    - device name
    - the number and (human readable) severity of any current alarms
    - SLA for the previous 5 minute period - expressed as a percentage.

The output must be formatted as a table, with aligned columns.

The script must accept at least one argument, which shows the help text for the application.


Environment
===========

Your solution will be evaluated on a modern Linux system.

 - Solution must work with Python version 2.7
 - Solution must work when run using Ubuntu Linux
 - Solution must only use the Python standard library (no external dependencies)


Good luck.
