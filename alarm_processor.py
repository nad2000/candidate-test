#!/usr/bin/env python
"""
Task 1 - Maintain current alarm table
=====================================

Write code that processes alarm events as they occur and maintains
the current_alarm table of alarms. This table reflects the current state of
all alarms that have not yet been cleared.

To determine if a new alarm event matches an existing current alarm
the 'device_name' and 'alarm_name' fields are compared. If they match,
they refer to the same alarm.

An alarm event can have a severity in the range 0 - 5:
0: CLEARED
1: SUPERVISORY
2: WARNING
3: MINOR
4: MAJOR
5: CRITICAL

An alarm is considered to be 'cleared' if the severity is 0.
Cleared alarms are no longer considered current, so need to be removed
from the current_alarm table. All other values are in ascending order of
severity, with CRITICAL being the most severe.

An alarm is considered to be 'updated' if the severity changes but
the device_name and alarm_name remain unchanged. An updated alarm replaces
the existing current alarm.

Of course, any new alarms are just added to the table.

Duplicate current alarms are not allowed. The solution should ensure that
any alarm events are continuously processed and that the current alarm table
is continuously updated, i.e. not exit until Ctrl-C is pressed.

Task 2 - Statistics extension
=============================

Extend your solution to include some basic metrics for each device.
The list of all devices can be be deduced by looking at the alarm_stream.py script.

These metrics must be stored in the same database as the alarm events.
Changes to the schema of the database must be clearly identified.

Each metric value must be computed and stored using a 5 minute interval.
A maximum of 3 days of historical metric values must be stored - any older
values must be deleted.

1. Alarm count
    The number of alarm events received per device, per severity.
2. SLA
    The amount of time that the device did *not* have a CRITICAL alarm present
    in the current alarm table for the metric interval.


@author: rad@nowitworks.eu
@url: http://hg.nowitworks.eu/candidate-test

"""

from alarm_stream import DB_NAME
import sqlite3
import time


def run_script(script):
    """Run SQL script until succeed, doesn't get locked."""

    global connection

    while True:

        try:
            connection.executescript(script)
            connection.commit()
            return

        except sqlite3.OperationalError:
            pass


def init_db():
    """Sets up DB for collectin metrics"""

    global connection

    run_script("""
        CREATE TABLE IF NOT EXISTS alarm_count (
            timestamp_utc integer,
            -- The number of alarm events received per device, per severity
            device_name text,
            alarm_severity integer,
            count integer
        );

        CREATE TABLE IF NOT EXISTS sla_stats (
          timestamp_utc integer,
          device_name text,
          -- The amount of time that the device did not have a CRITICAL alarm present
          sla integer
        );""")


def process():
    """Process alarm events until Ctrl-C"""

    global connection

    try:
        cursor = connection.cursor()
        cursor.execute("SELECT MIN(alarm_event_id) FROM alarm_event")
        start_id = cursor.fetchone()[0]

        while True:
            try:
                cursor.execute("SELECT MAX(alarm_event_id) FROM alarm_event")
                end_id = cursor.fetchone()[0]

                if start_id == end_id:  # no new evnts
                    time.sleep(1)
                    continue

                run_script("""
                    REPLACE INTO current_alarm
                      (timestamp_utc, device_name, alarm_name, alarm_severity)
                    SELECT
                      ae.timestamp_utc,
                      ae.device_name,
                      ae.alarm_name,
                      ae.alarm_severity
                    FROM (
                      SELECT MAX(alarm_event_id) AS id
                      FROM alarm_event
                      WHERE alarm_event_id BETWEEN %d AND %d
                      GROUP BY device_name, alarm_name
                      -- HAVING alarm_severity > 0
                    ) AS ids
                    JOIN alarm_event AS ae ON alarm_event_id = id
                    LEFT JOIN current_alarm AS ca
                        ON  ca.device_name = ae.device_name
                            AND ca.alarm_name = ae.alarm_name
                            AND ca.alarm_severity <> ae.alarm_severity;

                    -- Remove "cleared" alarms
                    DELETE FROM current_alarm
                    WHERE alarm_severity = 0;""" % (start_id, end_id))

                # collect stats every 5 minutes
                if int(time.time()) % 300 == 0:
                    collect_stats()

                start_id = end_id
                time.sleep(1)

            except sqlite3.OperationalError:
                pass

    except KeyboardInterrupt:
        print 'done'

    finally:
        connection.commit()
        connection.close()


def collect_stats():
    """Collect alarm count and SLA stats"""

    run_script("""
        -- Delete historical records (older then 3 days):
        DELETE FROM alarm_count
        WHERE timestamp_utc <= (strftime('%s', 'now') - 4320); -- 3*24*60

        DELETE FROM sla_stats
        WHERE timestamp_utc <= (strftime('%s', 'now') - 4320);

        INSERT INTO alarm_count
        SELECT
          (timestamp_utc/300)*300,
          device_name,
          alarm_severity,
          count(*) AS count
        FROM alarm_event
        WHERE
          timestamp_utc < (strftime('%s', 'now')/300)*300
          AND
          timestamp_utc >= (SELECT ifnull(MAX(timestamp_utc)+300,-1)
                            FROM alarm_count)
        GROUP BY (timestamp_utc/300)*300, device_name, alarm_severity;

        INSERT INTO sla_stats
        SELECT
          (timestamp_utc/300)*300,
          device_name,
          300-MAX(timestamp_utc-(timestamp_utc/300)*300) AS sla
        FROM alarm_event
        WHERE
          alarm_severity = 5 -- CRITICAL
          AND
          timestamp_utc < (strftime('%s', 'now')/300)*300
          AND
          timestamp_utc >= (SELECT ifnull(MAX(timestamp_utc)+300,-1)
                            FROM sla_stats)
        GROUP BY (timestamp_utc/300)*300, device_name;""")

if __name__ == '__main__':
    print 'Current alarm table maintenance in %s' % DB_NAME

    connection = sqlite3.connect(DB_NAME)

    init_db()
    process()
